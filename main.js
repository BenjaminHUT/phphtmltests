$(document).ready(function () {

    $(".drop").click(function () {

        $(".lists").slideToggle(100);

    });


    $("#submitfinal").click(function () {


        let jso = $("form").serializeArray();
        let newJson = {}
        for (entry of jso) {
            let { name, value } = entry;
            newJson[name] = value
        }

        for (i = 1; i <= 4; i++) {
            i = i.toString();

            if (newJson["pt" + i] == undefined) {
                let key = "pt" + i;
                $.extend(newJson, { [key]: 0 });
            }
            else {
            };
        };

        let data = JSON.stringify(newJson)
        sendRequest(data, "submit")

    });

    $("#loadquery").click(function () {
        sendRequest("Null", "loaddata");
    });

    function queryload(data) {
        console.log(data)
        data = JSON.parse(data)
        $("#testdropdown").val(data["dropdownmenu"]);
        $("#nameinput").val(data["Nameinput"]);
        for (i = 1; i <= 4; i++) {
            i = i.toString();

            if (data["pt" + i] == 0 || data["pt" + i] == "0") {
                $('#pr' + i).prop('checked', false);
            }
            else {
                $('#pr' + i).prop('checked', true);
            }



        }
    };



    function sendRequest(data, method) {
        let loc = location.toString().slice(0, -11) + "/ajax.php"
        $.ajax({
            type: "GET",
            url: loc,
            contentType: "application/json",
            data: { "form": data, "method": method },
            cache: false,
            success: function (data, status) {
                if (method == "loaddata") {
                    queryload(data);
                };
            }
        });
    }
});